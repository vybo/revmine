import csv
from datetime import datetime


def export_versions_with_scores(file_path, versions_with_scores):
    start_time = datetime.now()
    print ("[csv_export]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))
    with open(file_path, 'w') as csvfile:
        fieldnames = ['version', 'reviews_count', 'average_stars', '1_star_count', '2_star_count', '3_star_count',
                      '4_star_count', '5_star_count']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for version in versions_with_scores.keys():
            average_stars = 'none'
            one_star_count = 'none'
            two_star_count = 'none'
            three_star_count = 'none'
            four_star_count = 'none'
            five_star_count = 'none'

            if 'average_stars' in versions_with_scores[version].keys():
                average_stars = versions_with_scores[version]['average_stars']

            if 'stars' in versions_with_scores[version].keys():
                one_star_count = versions_with_scores[version]['stars']['1']
                two_star_count = versions_with_scores[version]['stars']['2']
                three_star_count = versions_with_scores[version]['stars']['3']
                four_star_count = versions_with_scores[version]['stars']['4']
                five_star_count = versions_with_scores[version]['stars']['5']

            writer.writerow({
                'version': version,
                'reviews_count': len(versions_with_scores[version]['reviews']),
                'average_stars': str(average_stars),
                '1_star_count': str(one_star_count),
                '2_star_count': str(two_star_count),
                '3_star_count': str(three_star_count),
                '4_star_count': str(four_star_count),
                '5_star_count': str(five_star_count)
            })

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[csv_export]Finished in " + str(difference_time_seconds) + ".")


def export_versions_with_classified_reviews_statistics(file_path, classified_versions):
    start_time = datetime.now()
    print ("[csv_export]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    with open(file_path, 'w') as csvfile:
        fieldnames = ['version', 'reviews_count', 'positive_reviews', 'negative_reviews']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for version in classified_versions.keys():
            positive_reviews = 0
            negative_reviews = 0

            for review in classified_versions[version]['reviews']:
                if review['sentiment'] == 'pos':
                    positive_reviews += 1
                if review['sentiment'] == 'neg':
                    negative_reviews += 1

            writer.writerow({
                'version': version,
                'reviews_count': len(classified_versions[version]['reviews']),
                'positive_reviews': str(positive_reviews),
                'negative_reviews': str(negative_reviews)
            })

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[csv_export]Finished in " + str(difference_time_seconds) + ".")


def export_classified_reviews(file_path, classified_versions):
    start_time = datetime.now()
    print ("[csv_export]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))
    with open(file_path, 'w') as csvfile:
        fieldnames = ['author', 'content', 'datetime', 'id', 'language', 'sentiment',
                      'stars', 'title', 'version']

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for version in classified_versions.keys():
            for review in classified_versions[version]['reviews']:
                writer.writerow({
                    'author': review['author'],
                    'content': review['content'],
                    'datetime': review['datetime'],
                    'id': review['id'],
                    'language': review['language'],
                    'sentiment': review['sentiment'],
                    'stars': review['stars'],
                    'title': review['title'],
                    'version': review['version']
                })

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[csv_export]Finished in " + str(difference_time_seconds) + ".")


def export_word_counts(file_path, counted_classified_versions_with_reviews):
    start_time = datetime.now()
    print ("[csv_export]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))
    with open(file_path, 'w') as csvfile:
        fieldnames = ['word']
        words = {}

        for version in counted_classified_versions_with_reviews.keys():
            fieldnames.append(version + '_pos')
            fieldnames.append(version + '_neg')

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for version in counted_classified_versions_with_reviews.keys():
            for word in counted_classified_versions_with_reviews[version]['words']['positive']:
                if word not in words.keys():
                    words[word] = {}

                words[word][version + '_pos'] = counted_classified_versions_with_reviews[version]['words']['positive'][word]

            for word in counted_classified_versions_with_reviews[version]['words']['negative']:
                if word not in words.keys():
                    words[word] = {}

                words[word][version + '_neg'] = counted_classified_versions_with_reviews[version]['words']['negative'][word]

        for word in words:
            row = {'word': word}

            for i in range(1, len(fieldnames)-1):
                # If word has version count, write it
                if fieldnames[i] in words[word].keys():
                    row[fieldnames[i]] = words[word][fieldnames[i]]
                else:
                    row[fieldnames[i]] = 0 # Otherwise it is not in a certain version and has count of 0

            writer.writerow(row)

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[csv_export]Finished in " + str(difference_time_seconds) + ".")