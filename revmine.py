import sys
import args_parser
import csv_import
import csv_export
import language_tools
from datetime import datetime
from textblob.classifiers import NaiveBayesClassifier


opts = args_parser.parse_args(sys.argv[1:])


def get_review_files():
    if "--input_file" in opts.keys():
        imported_file = csv_import.import_file_by_version(opts["--input_file"])
        return imported_file
    else:
        print ("No file specified for input\nUsage: revmine.py --input_file \"Path to file\"")


def get_classified_review_files():
    if "--input_file" in opts.keys():
        imported_file = csv_import.import_file_by_version(opts["--input_file"])
        return imported_file
    else:
        print ("No file specified for input\nUsage: revmine.py --input_file \"Path to file\"")


def count_version_scores(versions_with_reviews):
    for version in versions_with_reviews.keys():
        if 'average_stars' not in versions_with_reviews[version].keys():
            versions_with_reviews[version]['average_stars'] = 0.0

        if 'stars' not in versions_with_reviews[version].keys():
            versions_with_reviews[version]['stars'] = {}
            versions_with_reviews[version]['stars']['1'] = 0
            versions_with_reviews[version]['stars']['2'] = 0
            versions_with_reviews[version]['stars']['3'] = 0
            versions_with_reviews[version]['stars']['4'] = 0
            versions_with_reviews[version]['stars']['5'] = 0

        for review in versions_with_reviews[version]['reviews']:
            versions_with_reviews[version]['average_stars'] += float(review['stars'])
            versions_with_reviews[version]['stars'][review['stars']] += 1

        versions_with_reviews[version]['average_stars'] = versions_with_reviews[version]['average_stars'] \
                                                          / len(versions_with_reviews[version]['reviews'])

    return versions_with_reviews


def classify_sentiments(versions_with_reviews):
    training_set = []
    testing_set = []
    start_time = datetime.now()
    total_reviews = 0

    print("[revmine] Starting sentiment classification... at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    # Load all reviews already classified for training and testing, classifier needs special format - TextBlob
    for version in versions_with_reviews.keys():
        total_reviews += len(versions_with_reviews[version]['reviews'])
        for review in versions_with_reviews[version]['reviews']:
            # Strip sentiment of possible errors, whitespaces etc.
            review['sentiment'] = review['sentiment'].rstrip()

            if review['sentiment'] != 'none' and review['sentiment'] != 'non':
                # Texts need to be decoded because reviews contain utf_8 characters such as emojis
                training_set.append((language_tools.remove_stopwords_and_stem(review['title'].decode("utf_8"))
                                     + " "
                                     + language_tools.remove_stopwords_and_stem(review['content'].decode("utf_8")),
                                     review['sentiment']))

    # Split training set to half and use that for testing
    testing_set = training_set[len(training_set)/2:]

    # Train the classifier
    cl = NaiveBayesClassifier(training_set)

    r = 0
    v = 0
    # Classify non-classified reviews
    for version in versions_with_reviews.keys():
        for review in versions_with_reviews[version]['reviews']:
            if review['sentiment'] == 'none' or review['sentiment'] == 'non':
                if review['stars'] == '1':
                    review['sentiment'] = 'neg'
                else:
                    classified = cl.classify(language_tools.remove_stopwords_and_stem(review['title'].decode("utf_8"))
                                             + " "
                                             + language_tools.remove_stopwords_and_stem(review['content'].decode("utf_8")))
                    review['sentiment'] = classified

            r += 1
            if r % 100 == 0:
                end_time = datetime.now()
                difference_time_seconds = end_time - start_time
                print("[revmine] Classified review #" + str(r) + ". Progress: " + str((float(r) / float(total_reviews)) * 100) + ". Running time: " + str(difference_time_seconds))


        v += 1
        end_time = datetime.now()
        difference_time_seconds = end_time - start_time
        print("[revmine] Classified version #" + str(v) + "(" + version + "). Progress: " + str((float(r) / float(total_reviews)) * 100) + ". Running time: " + str(difference_time_seconds))


    # Check the accuracy on the test set
    print("Classifying accuracy was " + str(cl.accuracy(testing_set)))

    # Most informative features
    cl.show_informative_features(50)

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[revmine] Finished in " + str(difference_time_seconds) + ".")
    return versions_with_reviews


def count_words_in_reviews(classified_versions_with_reviews):
    start_time = datetime.now()
    print("[revmine] Starting word counting in reviews... at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    for version in classified_versions_with_reviews.keys():
        if 'words' not in classified_versions_with_reviews[version].keys():
            classified_versions_with_reviews[version]['words'] = {}
            classified_versions_with_reviews[version]['words']['positive'] = {}
            classified_versions_with_reviews[version]['words']['negative'] = {}

        for review in classified_versions_with_reviews[version]['reviews']:
            words = language_tools.remove_stopwords_and_stem(review['content'].decode("utf_8")).split()
            words.extend(language_tools.remove_stopwords_and_stem(review['title'].decode("utf_8")).split())
            # Remove duplicates
            words = list(set(words))

            # Switch for negative or positive
            sentiment = 'positive'
            if review['sentiment'] == 'neg':
                sentiment = 'negative'

            for word in words:
                if word not in classified_versions_with_reviews[version]['words'][sentiment]:
                    classified_versions_with_reviews[version]['words'][sentiment][word] = 1
                else:
                    classified_versions_with_reviews[version]['words'][sentiment][word] += 1

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[revmine] Finished in " + str(difference_time_seconds) + ".")
    return classified_versions_with_reviews


if "--mode" in opts.keys():
    mode = opts["--mode"]
    print("Mode found: " + mode)

    if mode == "--count_stars":
        print("Starting counting stars.")
        if "--output_file" in opts.keys():
            versions_with_reviews = get_review_files()
            versions_with_reviews_and_scores = count_version_scores(versions_with_reviews)
            csv_export.export_versions_with_scores(opts["--output_file"], versions_with_reviews_and_scores)
        else:
            print ("No file specified for export\nUsage: --output_file \"Path to file\"")

    elif mode == "naive_bayes_statistics":
        if "--output_file" in opts.keys():
            versions_with_reviews = get_review_files()
            print("Classifying sentiment with naive_bayes")
            classified_versions_with_reviews = classify_sentiments(versions_with_reviews)
            csv_export.export_versions_with_classified_reviews_statistics(opts["--output_file"], classified_versions_with_reviews)
            print("Classification done.")
        else:
            print ("No file specified for export\nUsage: --output_file \"Path to file\"")

    elif mode == "naive_bayes_export":
        if "--output_file" in opts.keys():
            versions_with_reviews = get_review_files()
            print("Classifying sentiment with naive_bayes")
            classified_versions_with_reviews = classify_sentiments(versions_with_reviews)
            csv_export.export_classified_reviews(opts["--output_file"], classified_versions_with_reviews)
            print("Classification done.")
        else:
            print ("No file specified for export\nUsage: --output_file \"Path to file\"")

    elif mode == "count_words":
        if "--output_file" in opts.keys():
            classified_versions_with_reviews = get_classified_review_files()
            print("Counting words for versions, positive and negative.")
            counted_words = count_words_in_reviews(classified_versions_with_reviews)
            csv_export.export_word_counts(opts["--output_file"], counted_words)
            print("Counting done.")
        else:
            print ("No file specified for export\nUsage: --output_file \"Path to file\"")
    else:
        print("Mode is unknown, returning.")

else:
    print("Mode not found, running debug. Possible modes: count_stars, naive_bayes_statistics, naive_bayes_export, count_words")
    versions_with_reviews = get_review_files()
    print("Debug all done.")
