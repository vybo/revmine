import re
import stopwords
from nltk import PorterStemmer

#stemmer = PorterStemmer()


def remove_stopwords_and_stem(from_string):
    stopword_pattern = re.compile(r'\b(' + r'|'.join(stopwords.stop_words) + r')\b\s*')
    emoji_pattern = re.compile(
        u"(\ud83d[\ude00-\ude4f])|"  # emoticons
        u"(\ud83c[\udf00-\uffff])|"  # symbols & pictographs (1 of 2)
        u"(\ud83d[\u0000-\uddff])|"  # symbols & pictographs (2 of 2)
        u"(\ud83d[\ude80-\udeff])|"  # transport & map symbols
        u"(\ud83c[\udde0-\uddff])"  # flags (iOS)
        "+", flags=re.UNICODE)

    non_alpha_pattern = re.compile('[^a-zA-Z]')

    text = emoji_pattern.sub('', from_string)

    words_array = text.split()
    stemmed_words_array = []

    for word in words_array:
        word = non_alpha_pattern.sub('', word)
        #word = stemmer.stem(word.lower())
        word = stopword_pattern.sub('', word.lower())
        if word != '':
            stemmed_words_array.append(word)

    return ' '.join(stemmed_words_array)
