from datetime import datetime
import csv


def import_file_by_version(csv_file_path):
    start_time = datetime.now()
    print ("[csv_import]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))
    version_dict = {}

    with open(csv_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # Add new version dict
            if row['version'] not in version_dict.keys():
                version_dict[row['version']] = {}
                version_dict[row['version']]['version'] = row['version']
                version_dict[row['version']]['reviews'] = []

            version_dict[row['version']]['reviews'].append(row)

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[csv_import]Finished in " + str(difference_time_seconds) + ".")
    return version_dict

